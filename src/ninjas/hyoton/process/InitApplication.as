﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.hyoton.process 
{
    import ninjas.hyoton.display.DisplayList;
    import ninjas.hyoton.vo.VideoVO;
    
    import system.ioc.ObjectFactory;
    import system.process.Initializer;
    
    import vegas.models.CoreModelObject;
    import vegas.net.FlashVars;
    
    /**
     * This process initialize the application.
     */
    public class InitApplication extends Initializer 
    {
        /**
         * Creates a new InitApplication instance.
         */
        public function InitApplication()
        {
            //
        }
        
        /**
         * The ioc factory reference.
         */
        public var factory:ObjectFactory ;
        
        /**
         * The flashvars reference.
         */
        public var flashvars:FlashVars ;
        
        /**
         * The video model reference.
         */
        public var model:CoreModelObject ;
        
        /**
         * The source of the video to load.
         */
        public var source:String ;
        
        /**
         * Initialize the application.
         */
        public function initialize():void
        {
            logger.debug(this + " initialize.") ;
            
            if ( flashvars )
            {
                if( flashvars.contains("source") )
                {
                    source = flashvars.getValue("source") ;
                }
            }
            else
            {
                logger.warn(this + " failed, the flashvars reference not must be null.") ;
            }
            
            logger.info(this + " source : " + source ) ;
            
            if ( factory )
            {
                factory.getObject( DisplayList.APPLICATION ) ;
            }
            else
            {
                logger.warn(this + " failed, the factory reference not must be null.") ;
            }
            
            if ( model )
            {
                if ( source != null && source != "" )
                {
                    var video:VideoVO =  new VideoVO() ;
                    
                    video.source = source ;
                    
                    model.setCurrentVO( video ) ;
                }
                else
                {
                    logger.warn(this + " failed, no source defines to run the video of the player.") ;
                }
            }
            else
            {
                logger.warn(this + " failed, the model reference not must be null.") ;
            }
        }
    }
}