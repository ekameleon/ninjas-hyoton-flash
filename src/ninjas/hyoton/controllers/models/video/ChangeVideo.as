﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.hyoton.controllers.models.video
{
    import lunas.components.buttons.LightFrameLabelButton;
    
    import ninjas.hyoton.vo.VideoVO;
    
    import vegas.controllers.AbstractController;
    import vegas.events.ModelObjectEvent;
    import vegas.net.NetStreamExpert;
    
    import flash.events.Event;
    import flash.text.TextField;
    
    /**
     * Invoked when a video value object is selected in the model.
     */
    public class ChangeVideo extends AbstractController
    {
        /**
         * Creates a new ChangeVideo instance.
         */
        public function ChangeVideo() 
        {
            //
        }
        
        /**
         * The default time expression.
         */
        public var defaultTime:String = "00:00" ;
        
        /**
         * The video duration textfield reference.
         */
        public var duration:TextField ;
        
        /**
         * The IoC factory reference.
         */
        public var expert:NetStreamExpert ;
        
        /**
         * The pause button reference.
         */
        public var pauseButton:LightFrameLabelButton ;
        
        /**
         * The play button reference.
         */
        public var playButton:LightFrameLabelButton ;
        
        /**
         * The video time textfield reference.
         */
        public var time:TextField ;
        
        /**
         * Handles the event.
         */
        public override function handleEvent(e : Event):void 
        {
            var video:VideoVO = ModelObjectEvent(e).getVO() as VideoVO ;
            
            var source:String = video.source ;
            
            logger.debug( this + " handleEvent video:" + video + " source:" + source ) ;
            
            if ( duration )
            {
                duration.htmlText = defaultTime ; 
            }
            
            if ( pauseButton )
            {
                pauseButton.visible = true  ;
            }
            
            if ( playButton )
            {
                playButton.visible = false  ;
            }
            
            if ( time )
            {
                time.htmlText = defaultTime ; 
            }
            
            if ( expert )
            {
                expert.close() ;
                expert.play( source ) ;
            }
        }
    }
}