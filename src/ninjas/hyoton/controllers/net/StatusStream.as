﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.hyoton.controllers.net
{
    import lunas.components.buttons.LightFrameLabelButton;
    
    import system.eden;
    
    import vegas.controllers.AbstractController;
    import vegas.net.NetStreamExpert;
    import vegas.net.NetStreamStatus;
    
    import flash.events.Event;
    import flash.events.NetStatusEvent;
    
    /**
     * Invoked when the stream status is changed.
     */
    public class StatusStream extends AbstractController 
    {
        /**
         * Creates a new StatusStream instance.
         */
        public function StatusStream()
        {
            super();
        }
        
        /**
         * The expert reference.
         */
        public var expert:NetStreamExpert ;
        
        /**
         * Indicates if the stream is looping or not when the playing is finished.
         */
        public var loop:Boolean = true ;
        
        /**
         * The pause button reference.
         */
        public var pauseButton:LightFrameLabelButton ; 
        
        /**
         * The play button reference.
         */
        public var playButton:LightFrameLabelButton ; 
        
        /**
         * Handles the event.
         */
        public override function handleEvent( e:Event ):void 
        {
            var ev:NetStatusEvent = e as NetStatusEvent ;
            if ( ev == null )
            {
                logger.warn( this + " handleEvent failed with, the handle event must be a NetStatusEvent." ) ;
                return ;
            }
            logger.debug( this + " handleEvent : " + eden.serialize(ev.info) ) ;
            switch( ev.info.code )
            {
                case NetStreamStatus.PLAY_START.code :
                {
                    if ( pauseButton )
                    {
                        pauseButton.visible = true  ;
                    }
                    
                    if ( playButton )
                    {
                        playButton.visible = false ;
                    }
                    break ;
                }
                case NetStreamStatus.PLAY_STOP.code :
                {
                    if ( expert )
                    {
                        if ( loop )
                        {
                            expert.seek(0) ;
                        }
                        else
                        {
                            //
                        }
                    }
                    if ( pauseButton )
                    {
                        pauseButton.visible = loop  ;
                    }
                    
                    if ( playButton )
                    {
                        playButton.visible = !loop ;
                    }
                    break ;
                }
            }
        }
    }
}
