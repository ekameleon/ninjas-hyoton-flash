﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.hyoton.controllers.net
{
    import lunas.components.bars.LevelBar;

    import ninjas.hyoton.logging.logger;

    import system.Strings;
    import system.signals.Receiver;

    import vegas.date.Time;
    import vegas.media.FLVMetaData;

    import flash.text.TextField;

    /**
     * This receiver is used when a NetStream is playing 
     * and this NetStreamClient emit a FLVMetaData object.
     */
    public class MetaDataChange implements Receiver 
    {
        /**
         * Creates a new MetaDataChange instance.
         */
        public function MetaDataChange()
        {
            // 
        }
        
        /**
         * Indicates the progress display item.
         */
        public var bar:LevelBar ;
        
        /**
         * The video duration textfield reference.
         */
        public var durationField:TextField ;
        
        /**
         * The video duration pattern.
         */
        public var durationPattern:String ;
        
        /**
         * This method is called when the receiver is connected with a Signal object.
         * @param ...values All the values emitting by the signals connected with this object.
         */
        public function receive( ...values:Array ):void
        {
            var datas:FLVMetaData = values[0] as FLVMetaData ;
            if ( datas )
            {
                var duration:Number = isNaN(datas.duration) ? 100 : datas.duration ;
                
                logger.debug( this + " receive duration:" + duration ) ;
                
                if ( bar )
                {
                    bar.maximumLevel = duration ; 
                }
                if ( durationField )
                {
                    var current:String ;
                    var time:Time      = new Time() ; 
                    var m:Number ;
                    var s:Number ;
                    
                    time.setValue( duration , "s" ) ;
                    
                    m = time.getMinutes( 0 ) ;
                    s = time.getSeconds( 0 ) ;
                    
                    current = "" ;
                    if ( durationPattern )
                    {
                        current = Strings.format
                        ( 
                            durationPattern , 
                            (m <= 9) ? "0" + m :  m , 
                            (s <= 9) ? "0" + s :  s  
                        ) ;
                    }
                    durationField.htmlText = current ;
                }
            }
            else
            {
                logger.warn(this + " receive failed, the passed-in metaData not must be null.") ;
            }
        }
    }
}
