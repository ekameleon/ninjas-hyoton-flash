﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.hyoton.controllers.display 
{
    import lunas.Button;

    import ninjas.hyoton.display.application.VideoPlayer;
    import ninjas.hyoton.display.application.VideoProtector;

    import vegas.controllers.AbstractController;
    import vegas.display.Background;

    import flash.display.Stage;
    import flash.display.StageDisplayState;
    import flash.events.Event;
    import flash.media.Video;

    /**
     * Invoked when the stage fullscreen mode is changed.
     */
    public class FullScreen extends AbstractController 
    {
        /**
         * Creates a new FullScreen instance.
         */
        public function FullScreen()
        {
            super();
        }
        
        /**
         * The fullscreen button reference.
         */
        public var button:Button ;
        
        /**
         * The video player reference.
         */
        public var player:VideoPlayer ;
        
        /**
         * The video protector reference.
         */
        public var protector:VideoProtector ;
        
        /**
         * The remote reference.
         */
        public var remote:Background ;
        
        /**
         * The stage reference.
         */
        public var stage:Stage ;
        
        /**
         * The video reference.
         */
        public var video:Video ;
        
        /**
         * Handles the event.
         */
        public override function handleEvent( e:Event ):void
        {
            if ( stage )
            {
                logger.debug(this + " handleEvent.") ;
                var isFull:Boolean = stage && stage.displayState == StageDisplayState.FULL_SCREEN ;
                if ( button != null )
                {
                    button.setSelected( isFull, true ) ;
                }
                if ( protector )
                {
                    if ( isFull )
                    {
                        stage.addChild( protector ) ;
                        protector.video  = video ;
                        protector.remote = remote ;
                    }
                    else if ( stage.contains( protector ) )
                    {
                        stage.removeChild( protector ) ;
                        protector.dispose() ;
                        if ( remote )
                        {
                            remote.visible = true ;
                        }
                        if ( player )
                        {
                            player.update() ;
                        }
                    }
                }
            }
            else
            {
                logger.warn(this + " handleEvent failed, the stage reference not must be null.") ;
            }
        }
    }
}
