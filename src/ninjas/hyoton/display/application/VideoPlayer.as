﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.hyoton.display.application 
{
    import core.maths.replaceNaN;

    import graphics.Align;
    import graphics.display.DisplayObjects;
    import graphics.geom.EdgeMetrics;

    import vegas.display.Background;

    import flash.geom.Rectangle;
    import flash.media.Video;

    /**
     * The video player display.
     */
    public class VideoPlayer extends Background 
    {
        /**
         * Creates a new VideoPlayer instance.
         */
        public function VideoPlayer()
        {
            //
        }
        
        /**
         * The internal padding of the player.
         */
        public var padding:EdgeMetrics = new EdgeMetrics() ;
        
        /**
         * the remote reference.
         */
        public var remote:Background ;
        
        /**
         * The video reference.
         */
        public var video:Video ;
        
        /**
         * Invoked when the view is changed.
         */
        public override function viewChanged():void
        {
            var b:Number , l:Number , r:Number , t:Number ;
            
            if ( padding )
            {
                b = replaceNaN( padding.bottom ) ;
                l = replaceNaN( padding.left   ) ;
                r = replaceNaN( padding.right  ) ;
                t = replaceNaN( padding.top    ) ;
            }
            else
            {
                b = 0 ;
                l = 0 ;
                r = 0 ;
                t = 0 ;
            }
            
            if ( video )
            {
                video.x = l ;
                video.y = t ;
                video.width  = w - ( l + r ) ;
                video.height = h - ( t + b ) ;
            }
            if ( remote )
            {
                DisplayObjects.align( remote , new Rectangle(video.x, video.y,video.width,video.height) , Align.BOTTOM , null, "w" , "h" ) ;
            }
        }
    }
}
