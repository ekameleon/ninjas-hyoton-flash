﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package ninjas.hyoton.context.net 
{
    public const stream:Array =
    [
        // net connection
        
        {
            id         : "video_net_connection" ,
            type       : "vegas.net.Connection" ,
            singleton  : true ,
            lazyInit   : true ,
            properties :
            [
                { name : "connect" , arguments:[ {value:null} ] }
            ]
        }
        ,
        
        // video net stream
        
        {
            id        : "video_net_stream" ,
            type      : "flash.net.NetStream" ,
            singleton : true ,
            lazyInit  : true ,
            arguments : [ { ref:"video_net_connection" } ] 
        }
        ,
        
        // video stream timer
        
        {
            id         : "video_stream_timer" ,
            type       : "flash.utils.Timer" ,
            singleton  : true ,
            lazyInit   : true , 
            arguments  : [ { config : "video_stream_timer" } ]
        }
        ,
        
        // video stream expert
        
        {
            id         : "video_progress_listener" ,
            type       : "{controllers}.net.ProgressStream" ,
            singleton  : true  ,
            lazyInit   : true  ,
            properties :
            [
                { name : "bar"             , ref    : "video_level_bar"     } ,
                { name : "expert"          , ref    : "video_stream_expert" } ,
                { name : "timeField"       , ref    : "video_time"          } ,
                { name : "timePattern"     , locale : "video_time.pattern"  } 
            ]
            ,
            listeners : 
            [ 
                { dispatcher : "video_stream_timer" , type : "timer"                                     } ,
                { dispatcher : "video_level_bar"    , type : "press"          , method : "startDragging" } ,
                { dispatcher : "video_level_bar"    , type : "release"        , method : "stopDragging"  } ,
                { dispatcher : "video_level_bar"    , type : "releaseOutside" , method : "stopDragging"  } 
            ]
        }
        ,
        
        {
            id        : "video_net_stream_listener" ,
            type      : "{controllers}.net.StatusStream" ,
            singleton : true ,
            lazyInit  : true ,
            properties : 
            [ 
                { name : "loop"        , config : "loop"                } ,
                { name : "expert"      , ref    : "video_stream_expert" } ,
                { name : "pauseButton" , ref    : "video_pause_button"  } ,
                { name : "playButton"  , ref    : "video_play_button"   } 
            ]
            ,
            listeners :
            [
                { dispatcher : "video_net_stream" , type : "netStatus" }
            ]
        }
        ,
        {
            id         : "video_metadata_receiver" ,
            type       : "{controllers}.net.MetaDataChange" ,
            singleton  : true ,
            lazyInit   : true ,
            receivers : [ { signal : "video_stream_expert.meta" } ] ,
            properties : 
            [ 
                { name : "bar"             , ref    : "video_level_bar"         } ,
                { name : "durationField"   , ref    : "video_duration"          } ,
                { name : "durationPattern" , locale : "video_duration.pattern"  } 
            ]
        }
        ,
        {
            id         : "video_stream_expert" ,
            type       : "vegas.net.NetStreamExpert" ,
            singleton  : true ,
            lazyInit   : true ,
            dependsOn  : 
            [ 
                "video_metadata_receiver" , 
                "video_net_stream_listener" ,
                "video_progress_listener" 
            ] 
            , 
            properties : 
            [ 
                { name : "verbose"   , config : "verbose"            } , 
                { name : "netStream" , ref    : "video_net_stream"   } ,
                { name : "timer"     , ref    : "video_stream_timer" } ,
                { name : "video"     , ref    : "video"              } 
            ] 
        }
    ] ;
}