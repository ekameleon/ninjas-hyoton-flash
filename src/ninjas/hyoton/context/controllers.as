﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.hyoton.context 
{
    public const controllers:Array =
    [
        // ui
        {
            id         : "fullscreen_listener" ,
            type       : "{controllers}.display.FullScreen" ,
            singleton  : true  ,
            lazyInit   : true  ,
            properties :
            [
                { name : "button"    , ref : "video_fullscreen_button" } ,
                { name : "player"    , ref : "video_player"            } ,
                { name : "protector" , ref : "video_protector"         } ,
                { name : "remote"    , ref : "video_remote"            } ,
                { name : "stage"     , ref : "#stage"                  } ,
                { name : "video"     , ref : "video"                   } 
            ] 
            ,
            listeners : [ { dispatcher : "#stage" , type : "fullScreen" } ] 
        }
        ,
        // models
        { 
            id         : "change_video_controller" , 
            type       : "{controllers}.models.video.ChangeVideo" ,
            singleton  : true ,
            lazyInit   : true ,
            properties : 
            [
                { name : "defaultTime" , locale : "video_default_time"  } ,
                { name : "duration"    , ref    : "video_duration"      } ,
                { name : "expert"      , ref    : "video_stream_expert" } ,
                { name : "pauseButton" , ref    : "video_pause_button"  } ,
                { name : "playButton"  , ref    : "video_play_button"   } ,
                { name : "time"        , ref    : "video_time"          } 
                
            ] 
        } 
        ,
        // frontcontroller
        {
            id                  : "front_controller" ,
            type                : "FrontController" , 
            singleton           : true ,
            lazyInit            : true ,
            staticFactoryMethod : { type : "FrontController" , name : "getInstance" } ,
            properties          :
            [
               { name : "add" , arguments : [ { value : "changeVideo" } , { ref : "change_video_controller" } ] } 
            ]
        }
    ];
}