﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.hyoton
{
    import graphics.FillStyle;
    import graphics.LineStyle;

    import lunas.components.bars.LevelBar;
    import lunas.components.buttons.LightFrameLabelButton;

    import ninjas.hyoton.controllers.display.FullScreen;
    import ninjas.hyoton.controllers.display.SwitchFullScreen;
    import ninjas.hyoton.controllers.display.VideoOut;
    import ninjas.hyoton.controllers.display.VideoOver;
    import ninjas.hyoton.controllers.models.video.ChangeVideo;
    import ninjas.hyoton.controllers.net.MetaDataChange;
    import ninjas.hyoton.controllers.net.MuteStream;
    import ninjas.hyoton.controllers.net.PauseStream;
    import ninjas.hyoton.controllers.net.PlayStream;
    import ninjas.hyoton.controllers.net.ProgressStream;
    import ninjas.hyoton.controllers.net.SeekStream;
    import ninjas.hyoton.controllers.net.StatusStream;
    import ninjas.hyoton.controllers.net.UnmuteStream;
    import ninjas.hyoton.display.application.Body;
    import ninjas.hyoton.display.application.VideoPlayer;
    import ninjas.hyoton.display.application.VideoProtector;
    import ninjas.hyoton.logging.sos;
    import ninjas.hyoton.net.JavascriptProxy;
    import ninjas.hyoton.process.InitApplication;

    import system.Reflection;
    import system.evaluators.EdenEvaluator;
    import system.events.FrontController;
    import system.ioc.ObjectConfig;
    import system.ioc.factory;
    import system.logging.LoggerLevel;
    import system.logging.targets.SOSTarget;
    import system.process.Action;

    import vegas.display.Root;
    import vegas.models.CoreModelObject;
    import vegas.net.ApplicationLoader;
    import vegas.net.Connection;
    import vegas.net.NetStreamExpert;

    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;
    
    [SWF(width="620", height="357", frameRate="24", backgroundColor="#333333")]
    // -target-player=10.1 -static-link-runtime-shared-libraries=true -optimize=true
    
    /**
     * The main Application class of the application.
     */
    public class Application extends Root 
    {
        /**
         * Creates a new Application instance.
         */
        public function Application()
        {
            //
        }
       
        /**
         * The loader of the application.
         */
        public var loader:ApplicationLoader ;
        
        /**
         * The wait field.
         */
        public var field:TextField ;
        
        /**
         * Invoked when the display is added to the stage.
         */
        protected override function addedToStage( e:Event = null ):void
        {
            // stage
            
            stage.align                  = StageAlign.TOP_LEFT ; 
            stage.scaleMode              = StageScaleMode.NO_SCALE ;
            stage.showDefaultContextMenu = false ;
            
            // logging
            
            var noDebug : * = flashVars.getValue("debug") === "false" ;
            if ( !noDebug && !sos ) 
            {  
                sos = new SOSTarget( Reflection.getClassPackage(this) , 0xA2A2A2 ) ;
                sos.filters       = ["*"] ;
                sos.level         = LoggerLevel.ALL ;
                sos.includeLines  = true  ;
                sos.includeTime   = true  ;
            }
            
            // IoC
            
            factory.config.lazyInit = true ;
            
            factory.run( definitions ) ;
            
            factory.config.lazyInit = false ;
            
            var contextFile:String = flashVars.getValue("contextFile") || "application.eden" ;
            var contextPath:String = flashVars.getValue("contextPath") || "context/" ;
            
            loader = new ApplicationLoader(contextFile, contextPath , factory ) ;
            
            loader.startIt.connect( start ) ; 
            loader.finishIt.connect( finish ) ; 
            
            loader.root       = this ;
            loader.flashVars = flashVars ;
            loader.stage     = stage ;
            
            loader.run() ;
        }
        
        /**
         * Invoked when the IoC factory is initialized.
         */
        protected function finish( action:Action ):void
        {
            logger.info( this + " finish" ) ;
            
            // remove progres display
             
            removeChild(field) ;
            field = undefined ;
            
            ///// flashvars
            
            if( flashVars.contains("loop") )
            {
                factory.config.config.loop = flashVars.getValue("loop") === "true" ;
            }
            
            // initialize factory
            
            if ( factory.isDirty() )
            {
                factory.run() ;
            }
        }
        
        /**
         * Invoked when the IoC factory start the initialization.
         */
        protected function start( action:Action ):void
        {
            logger.info( this + " start" ) ;
            
            var progressColor:Number   = ( flashVars.getValue("progressColor", new EdenEvaluator()) || 0xFFFFFF ) as Number  ;
            var progressMessage:String = flashVars.getValue("progressMessage") || "Loading in progress..."  ;
            
            field                   = new TextField() ;
            field.autoSize          = TextFieldAutoSize.LEFT ;
            field.defaultTextFormat = new TextFormat( "Verdana", 12, progressColor, true ) ;
            field.selectable        = false ;
            field.x                 = 10 ;
            field.y                 = 10 ;
            field.text              = progressMessage ;
            
            addChild( field ) ;
        }
        
        ///////////////// factory
        
        // config
        
        factory.config = new ObjectConfig
        ({
            typePolicy   : "all" ,
            typeAliases  :
            [
                { alias : "Background"            , type : "vegas.display.Background"                       } ,
                { alias : "LightFrameLabelButton" , type : "lunas.components.buttons.LightFrameLabelButton" } ,
                { alias : "TextField"             , type : "flash.text.TextField"                           } ,
                { alias : "FrontController"       , type : "system.events.FrontController"                  } 
            ]
            ,
            typeExpression  :
            [
                { name : "player"      , value : "ninjas.hyoton"        } ,
                { name : "display"     , value : "{player}.display"     } ,
                { name : "controllers" , value : "{player}.controllers" } 
            ]
        }) ;
        
        ////////////// Linkage enforcer
        
        // graphics
        
        LineStyle ; FillStyle ;
        
        // vegas
        
        CoreModelObject ;
        
        Connection ; NetStreamExpert ;
        
        // lunas
        
        LevelBar ; LightFrameLabelButton ;
        
        // system
        
        FrontController ;
        
        // ninjas.hyoton
        
        FullScreen ; SwitchFullScreen ; VideoOut ; VideoOver ;
        ChangeVideo  ;
        MetaDataChange ; MuteStream ; PauseStream  ; PlayStream ; ProgressStream ; 
        SeekStream ; StatusStream ; UnmuteStream ;
        
        Body ; VideoPlayer ; VideoProtector ;
        
        JavascriptProxy ;
        
        InitApplication ;
    }
}
