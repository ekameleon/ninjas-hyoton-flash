﻿/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  
           http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is NinjAS Framework.
  
  The Initial Developer of the Original Code is 
  ALCARAZ Marc <ekameleon@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2009-2011
  the Initial Developer. All Rights Reserved.
  
  Contributor(s) :
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/

package ninjas.hyoton.net 
{
    import ninjas.hyoton.logging.logger;
    
    import system.process.Runnable;
    
    import flash.external.ExternalInterface;
    import flash.system.Capabilities;
    
    /**
     * Javascript proxy access class.
     */
    public class JavascriptProxy implements Runnable
    {
        /**
         * Creates a new JavascriptProxy instance.
         */
        public function JavascriptProxy()
        {
            //
        }
        
        /**
         * A Javascript injection block (String or XML) to execute without any parameters and without return values.
         */
        public var data:* ;
        
        /**
         * Indicates the verbose mode.
         */
        public var verbose:Boolean = true ;
        
        /**
         * Indicates if the javascript proxy is available.
         */
        public function get available():Boolean
        {
            var b:Boolean = ExternalInterface.available;
            if( b && (Capabilities.playerType == "External") )
            {
                b = false ;
            }
            return b ;
        }
        
        /**
         * Call a Javascript injection block (String or XML) with parameters and return the result.
         */
        public function call( functionName:String, ...args:Array ):*
        {
            if( available )
            {
                try
                {
                    args.unshift( functionName );
                    return ExternalInterface.call.apply( ExternalInterface, args );
                }
                catch( e:SecurityError )
                {
                    if ( verbose )
                    {
                        logger.warn( this + " call method failed, the ExternalInterface is not allowed.\nEnsure that allowScriptAccess is set to \"always\" in the Flash embed HTML." );
                    }
                }
                catch( e:Error )
                {
                    if ( verbose )
                    {
                        logger.warn( "call method failed, ExternalInterface failed to make the call\nreason: " + e.message ) ;
                    }
                }
            }
            else
            {
                logger.warn( "call method failed, the proxy isn't available to call a javascript function." ) ;
            }
            return null;
        }
        
        /**
         * Returns the value property defines with the passed-in name value.
         * @return the value property defines with the passed-in name value.
         */
        public function getProperty( name:String ):*
        {
            return call( name + ".valueOf" ) ;
        }
        
        /**
         * Returns the String property defines with the passed-in name value.
         * @return the String property defines with the passed-in name value.
         */
        public function getPropertyString( name:String ):String
        {
            return call( name + ".toString" ); 
        }
        
        /**
         * Execute a Javascript injection block (String or XML) without any parameters and without return values.
         */
        public function run( ...arguments:Array ):void
        {
            if( available )
            {
                var block:* ;
                if ( arguments[0] && ( arguments[0] is String || arguments[0] is XML ) )
                {
                    block = arguments[0] ;
                }
                else
                {
                    block = data ;
                }
                try
                {
                    ExternalInterface.call( block );
                }
                catch( e:SecurityError )
                {
                    if ( verbose )
                    {
                        logger.warn( this + " run method failed, the ExternalInterface is not allowed.\nEnsure that allowScriptAccess is set to \"always\" in the Flash embed HTML." );
                    }
                }
                catch( e:Error )
                {
                    if ( verbose )
                    {
                        logger.warn( this + " run method failed, ExternalInterface failed to make the call\nreason: " + e.message ) ;
                    }
                }
            }
        }   
    }
}