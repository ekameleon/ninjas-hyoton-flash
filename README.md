# Hyoton - Video Player #
    
## DESCRIPTION ##
    
The "Hyoton jutsu" (ice jutsu) is a pure ActionScript 3 video player with a external Javascript control based on the HTML5 video API.
    
## VERSION ##
    
1.0
    
## LICENCE ## 
    
Mozilla Public License 1.1 (MPL 1.1) (read Licence.txt)
    
## PROJECT PAGES ##
    
 * https://bitbucket.org/ekameleon/ninjas-hyoton-flash
 * https://bitbucket.org/ekameleon/vegas (dependency)
    
## ABOUT ##
    
 * Author : ALCARAZ Marc (eKameleon)
 * Link   : http://www.ooopener.com
 * Mail   : ekameleon@gmail.com

## THANKS ##

FDT, the best ActionScript editor Forever !

![FDT_Supported.png](https://bitbucket.org/repo/AEbB9b/images/525527175-FDT_Supported.png) 
    
 * PowerFlasher - FDT OpenSource Licence : http://fdt.powerflasher.com/